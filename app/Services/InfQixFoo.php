<?php
declare(strict_types = 1);

namespace App\Services;


use App\Core\Services\Interfaces\ServiceImplementationInterface;
use App\Service;

class InfQixFoo extends Service implements ServiceImplementationInterface
{
    public function __construct(int $number = null, array $divisors = null, string $separator = null)
    {
        parent::__construct($number, $divisors, $separator);
    }
}

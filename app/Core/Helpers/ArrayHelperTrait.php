<?php


namespace App\Core\Helpers;


trait ArrayHelperTrait
{
    public function convertNumberToArray(int $input): array
    {
        $array = [];
        do {
            array_push($array, $input % 10);
            $input = intval($input / 10);
        } while ($input != 0);

        return $array;
    }
}

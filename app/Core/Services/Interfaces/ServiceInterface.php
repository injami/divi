<?php
declare(strict_types = 1);

namespace App\Core\Services\Interfaces;


interface ServiceInterface
{
    public function __construct
    (
        int $number = null,
        array $divisors = null,
        string $separator = null
    );

    public function validate(int $input): void;

    public function convertNumberToArray(int $input): array;

    public function occurrences(int $number = null): self;

    public function division(int $number = null): self;

    public function getNumber(): int;

    public function setNumber(int $number): self;

    public function getResult(): string;

    public function setSeparator(string $separator): self;

    public function getSeparator(): string;

    public function setDivisors(array $divisors): self;

    public function getDivisors(): array;

    public function clearResult(): self;

    public function divisionBySumOfArray(int $number = null, string $endString = null): self;

    public function getEndingString(): string;
}

<?php
declare(strict_types = 1);

namespace App\Core\Container\Interfaces;


interface ContainerInterface
{
    public function createInstance
    (
        string $name,
        int $number = null,
        array $divisors = null,
        string $separator = null
    );
}

<?php
declare(strict_types = 1);

namespace App\Core\Container\Traits;


trait ContainerCollectorTrait
{
    protected array $services = [];

    public function __construct()
    {
        $this->getServiceList();
    }

    private function getServiceList(): void
    {
        $services = scandir(__DIR__ . '/../../../Services');

        foreach ($services as $service) {
            if (preg_match('#php#', $service)) {
                $serviceName = preg_replace('#.php#', '', $service);
                $this->services[$serviceName] = 'App\\Services\\' . $serviceName;
            }
        }
    }
}

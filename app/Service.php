<?php
declare(strict_types = 1);

namespace App;


use App\Core\Helpers\ArrayHelperTrait;
use App\Core\Services\Interfaces\ServiceInterface;
use Exception;

class Service implements ServiceInterface
{
    use ArrayHelperTrait;

    private array $result;

    private string $separator;

    private array $divisors;

    private int $number;

    private string $endString;

    public function __construct(int $number = null, array $divisors = null, string $separator = null)
    {
        $this->divisors = $divisors ? $divisors : [3 => 'Foo', 5 => 'Bar', 7 => 'Qix', 8 => 'Inf'];
        $this->separator = $separator ? $separator : ', ';
        $this->result = [];
        $this->number = $number ? $number : 3;
        $this->endString = 'Inf';
    }

    public function setNumber(int $number): self
    {
        $this->validate($number);
        $this->number = $number;

        return $this;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function validate(int $input): void
    {
        if (!filter_var($input,
            FILTER_VALIDATE_INT,
            ['options' => ['min_range' => 1]]))
        {
            throw new Exception('This is not an integer');
        }
    }

    public function division(int $number = null): self
    {
        $working_number = $number ? $number : $this->getNumber();
        $this->validate($working_number);
        foreach ($this->divisors as $key => $value) {
            if ($working_number % $key === 0) {
                array_push($this->result, $value);
            }
        }
        return $this;
    }

    public function occurrences(int $number = null): self
    {
        $working_number = $number ? $number : $this->getNumber();
        $this->validate($working_number);
        foreach ($this->convertNumberToArray($working_number) as $item) {
            if (array_key_exists($item, $this->divisors)) {
                array_push($this->result, $this->divisors[$item]);
            }
        }
        return $this;
    }

    public function divisionBySumOfArray(int $number = null, string $endString = null): self
    {
        $working_number = $number ? $number : $this->getNumber();
        $this->validate($working_number);
        $working_string = $endString ? $endString : $this->getEndingString();
        if(array_sum($this->convertNumberToArray($working_number)) % 8 === 0) {
            array_push($this->result, $working_string);
        }
        return $this;
    }

    public function getResult(): string
    {
        return implode($this->separator, $this->result);
    }

    public function setSeparator(string $separator): self
    {
        $this->separator = (string)$separator;

        return $this;
    }

    public function getSeparator(): string
    {
        return $this->separator;
    }

    public function setDivisors(array $divisors): self
    {
        $this->divisors = $divisors;

        return $this;
    }

    public function getDivisors(): array
    {
        return $this->divisors;
    }

    public function clearResult(): self
    {
        $this->result = [];
        return $this;
    }

    public function getEndingString(): string
    {
        return $this->endString;
    }
}

<?php
declare(strict_types = 1);

namespace App;

use App\Core\Container\Traits\ContainerCollectorTrait;
use App\Core\Container\Interfaces\ContainerInterface;
use Exception;
use ReflectionClass;
use ReflectionException;

class Container implements ContainerInterface
{
    use ContainerCollectorTrait;

    public function createInstance
    (
        string $name,
        int $number = null,
        array $divisors = null,
        string $separator = null
    )
    {

        if (!array_key_exists($name, $this->services)) {
            throw new Exception('No such service');
        }

        if(func_num_args() > 1) {
            return $this->withArgs($name, $args = func_get_args());
        } else {
            return $this->withoutArgs($name);
        }
    }

    private function withoutArgs(string $name)
    {
        try {
            $reflection = new ReflectionClass($this->services[$name]);
            return $reflection->newInstance();
        } catch (ReflectionException $e) {
            sprintf("Error happened when creating reflection without Args \"%s\"", $e->getMessage());
        }
    }

    private function withArgs(string $name, array $args)
    {
        try {
            $reflection = new ReflectionClass($this->services[$name]);
            array_shift($args);
            return $reflection->newInstanceArgs($args);
        } catch (ReflectionException $e) {
            sprintf("Error happened when creating reflection with Args \"%s\"", $e->getMessage());
        }
    }
}

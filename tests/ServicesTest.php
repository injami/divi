<?php
declare(strict_types = 1);

namespace tests;


use App\Container;
use App\Core\Services\Interfaces\ServiceImplementationInterface;
use Error;
use Exception;
use PHPUnit\Framework\TestCase;
use TypeError;

class ServicesTest extends TestCase
{
    private Container $container;

    private ServiceImplementationInterface $service;

    public function __construct()
    {
        $this->container = new Container();
        $this->service = $this->container->createInstance('FooBarQix');
        parent::__construct();
    }

    /** @test */
    public function serviceDivisionBy3()
    {
        // 3
        $service = $this->service;
        $service->setNumber(9)
                ->division();
        $this->assertEquals('Foo', $service->getResult());
    }

    /** @test */

    public function serviceDivisionBy5()
    {
        // 5
        $service = $this->service;
        $service->setNumber(20)
                ->division();
        $this->assertEquals('Bar', $service->getResult());
    }

    /** @test */

    public function serviceDivisionForDigitsWhichMayHaveMultipleDivisorsAtOnce()
    {
        // 3 5 7
        $service = $this->service;
        $service->setNumber(210)
                ->division();
        $this->assertEquals('Foo, Bar, Qix', $service->getResult());
    }

    /** @test */

    public function occurrencesTestWithoutDivisorsInASingleDigitForFoo()
    {
        // 3
        $service = $this->service;
        $service->setNumber(31)
                ->occurrences();
        $this->assertEquals('Foo', $service->getResult());
    }

    /** @test */

    public function combinedOccurrencesAndDivisorsTestForSingleDigit()
    {
        $service = $this->service;
        $service->setNumber(357)
                ->division()
                ->occurrences();
        $this->assertEquals('Foo, Qix, Qix, Bar, Foo', $service->getResult());
    }

    /** @test */
    public function expectErrorIfSetNumberNotAnInt()
    {
        $this->expectException(Error::class);
        $service = $this->service;
        $service->setNumber('abc');
    }

    /** @test */
    public function expectExceptionIfSetNumberNotAnInt()
    {
        $this->expectException(Exception::class);
        $service = $this->service;
        $service->setNumber((int)'abc');
    }

    /** @test */
    public function expectErrorIfDivisionNumberNotAnInt()
    {
        $this->expectException(Error::class);
        $service = $this->service;
        $service->division('abc');
    }

    /** @test */
    public function expectErrorIfOccurrencesNumberNotAnInt()
    {
        $this->expectException(Error::class);
        $service = $this->service;
        $service->occurrences('abc');
    }

    /** @test */

    public function exceptionIfDivisorsIsNotAnArray()
    {
        $this->expectException(TypeError::class);
        $service = $this->service;
        $service->setDivisors('123');
    }

    /** @test */

    public function testForDifferentSeparator()
    {
        $service = $this->service;
        $service->setNumber(357)
                ->setSeparator('#')
                ->division()
                ->occurrences();
        $separator = $service->getSeparator();

        $this->assertEquals('#', $separator);
        $this->assertEquals('Foo#Qix#Qix#Bar#Foo', $service->getResult());
    }

    public function testForDifferentDivisorsArray()
    {
        $service = $this->service;
        $service->setDivisors([3 => 'Aaa', 5 => 'Bbb', 7 => 'Fff'])
                ->occurrences(357);

        $divisors = $service->getDivisors();

        $this->assertEquals([3 => 'Aaa', 5 => 'Bbb', 7 => 'Fff'], $divisors);
        $this->assertEquals('Fff, Bbb, Aaa', $service->getResult());
    }
}

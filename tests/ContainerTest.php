<?php


use App\Container;
use App\Core\Container\Traits\ContainerCollectorTrait;
use App\Services\FooBarQix;
use App\Services\InfQixFoo;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    /** @test
     * @throws Exception
     */
    public function shouldThrowAnExceptionWhenServiceDoesNotExist()
    {
        $this->expectException(Exception::class);
        $container = new Container();
        $container->createInstance('Some name');
    }

    /** @test
     * @throws Exception
     */
    public function exceptionWithWrongTypesOfArgumentsInConstructor()
    {
        $this->expectException(TypeError::class);
        $container = new Container();
        $container->createInstance('InfQixFoo', '123', '123');
    }

    /** @test */

    public function containerShouldReturnAServiceInstance()
    {
        $container = new Container();
        $instance = $container->createInstance('FooBarQix');
        $this->assertTrue($instance instanceof FooBarQix);
    }

    /** @test */

    public function containerShouldReturnAServiceInstanceWithArgs()
    {
        $container = new Container();
        $instance = $container->createInstance('InfQixFoo', 5, [1 => 'Foo'], ';' );
        $this->assertTrue($instance instanceof InfQixFoo);
    }
}

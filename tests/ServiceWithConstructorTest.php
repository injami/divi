<?php

namespace tests;

use App\Container;
use App\Core\Services\Interfaces\ServiceImplementationInterface;
use PHPUnit\Framework\TestCase;

class ServiceWithConstructorTest extends TestCase
{
    private Container $container;

    private ServiceImplementationInterface $service;

    public function __construct()
    {
        $this->container = new Container();
        $this->service = $this->container->createInstance
        (
            'FooBarQix',
            6,
            [8 => 'Inf', 7 => "Qix", 3 => 'Foo'],
            '; '
        );
        parent::__construct();
    }

    /** @test */

    public function serviceDivisionBy3()
    {
        $service = $this->service;
        // 3
        $service->division();
        $this->assertEquals('Foo', $service->getResult());
    }

    /** @test */

    public function divisionAssignNumberMultipleAssertions()
    {
        $service = $this->service;
        // 7
        $service->division(14);
        $this->assertEquals('Qix', $service->getResult());
        $service->clearResult();
        // 8
        $service->division(16);
        $this->assertEquals('Inf', $service->getResult());
    }

    /** @test */
    public function occurrencesAssignNumberMultipleAssertions()
    {
        $service = $this->service;
        // 7
        $service->occurrences(47);
        $this->assertEquals('Qix', $service->getResult());
        $service->clearResult();
        // 3
        $service->occurrences(31);
        $this->assertEquals('Foo', $service->getResult());
    }

    /** @test */
    public function lastStepTestingFor8DivisorThroughSetNumber()
    {
        $service = $this->service;
        $service->setNumber(125)
                ->divisionBySumOfArray();
        $this->assertEquals('Inf', $service->getResult());
    }

    /** @test*/
    public function lastStepTestingFor8DivisorAndNumber7Occurrence()
    {
        $service = $this->service;
        $service->divisionBySumOfArray(17)
                ->occurrences(17);
        $this->assertEquals('Inf; Qix', $service->getResult());
    }
}
